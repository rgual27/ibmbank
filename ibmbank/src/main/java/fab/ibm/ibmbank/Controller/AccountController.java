package fab.ibm.ibmbank.Controller;

import fab.ibm.ibmbank.Dto.AccountDto;
import fab.ibm.ibmbank.Service.AccountService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    private static Logger logger = Logger.getLogger(AccountController.class);

    @RequestMapping(value = "/accountSolvent", method = RequestMethod.POST)
    public HashMap<String, Object> accountSolvent(@RequestBody AccountDto dto) {
        HashMap<String, Object> response = new HashMap<String, Object>();
        try {
            boolean enabled = accountService.accountSolvent(dto);
            response.put("habilitado", enabled);
            response.put("codigo", enabled ? "200" : "201");
            response.put("detalle", enabled ? "Cuenta solvente" : "Cuenta insolvente");
        } catch(Exception e) {
            logger.error("", e);
            response.put("detalle", e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/extractAmount", method = RequestMethod.POST)
    public HashMap<String, Object> extractAmount(@RequestBody AccountDto dto) {
        HashMap<String, Object> response = new HashMap<String, Object>();
        try {
            boolean success = accountService.extractAmount(dto);
            response.put("codigo", success ? "200" : "201");
            response.put("detalle", success ? "Extracción exitosa" : "Extracción fallida");
        } catch(Exception e) {
            logger.error("", e);
            response.put("detalle", e.getMessage());
        }
        return response;
    }
}
