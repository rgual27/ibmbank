package fab.ibm.ibmbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbmbankApplication {

    public static void main(String[] args) {
        SpringApplication.run(IbmbankApplication.class, args);
    }

}
