package fab.ibm.ibmbank.Dto;

public class AccountDto {
    private int accountGet;
    private int accountPut;
    private long amount;

    public int getAccountGet() {
        return accountGet;
    }

    public void setAccountGet(int accountGet) {
        this.accountGet = accountGet;
    }

    public int getAccountPut() {
        return accountPut;
    }

    public void setAccountPut(int accountPut) {
        this.accountPut = accountPut;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
