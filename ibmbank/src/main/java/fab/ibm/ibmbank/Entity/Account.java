package fab.ibm.ibmbank.Entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "ACCOUNT")
public class Account {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "ID_ACCOUNT", unique = true)
    private int idAccount;

    @Column(name = "AMOUNT")
    private long amount;

    @Column(name = "PERMISSION", unique = true)
    private int permission;

    public Account() {
    }

    public Account(int idAccount, long amount, int permission) {
        this.idAccount = idAccount;
        this.amount = amount;
        this.permission = permission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }
}
