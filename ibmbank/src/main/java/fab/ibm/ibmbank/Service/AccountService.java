package fab.ibm.ibmbank.Service;

import fab.ibm.ibmbank.Dto.AccountDto;

public interface AccountService {
    boolean accountSolvent(AccountDto accountDto);
    boolean extractAmount(AccountDto accountDto);
}
