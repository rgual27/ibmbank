package fab.ibm.ibmbank.Service;

import fab.ibm.ibmbank.Dao.AccountDao;
import fab.ibm.ibmbank.Dto.AccountDto;
import fab.ibm.ibmbank.Entity.Account;
import fab.ibm.ibmbank.Repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    public AccountDao accountDao;

    @Autowired
    public AccountRepository accountRepository;

    public boolean accountSolvent(AccountDto accountDto) {
        Account account = accountDao.findByIdAccount(accountDto.getAccountGet());
        return account != null && account.getAmount() >= accountDto.getAmount();
    }

    public boolean extractAmount(AccountDto accountDto) {
        Account result = null;
        Account account = accountDao.findByIdAccount(accountDto.getAccountGet());
        if (account != null) {
            account.setAmount(account.getAmount() - accountDto.getAmount());
            result = accountRepository.save(account);
        }
        return result != null;
    }


}
