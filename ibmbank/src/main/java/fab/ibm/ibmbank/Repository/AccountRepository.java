package fab.ibm.ibmbank.Repository;

import fab.ibm.ibmbank.Dto.AccountDto;
import fab.ibm.ibmbank.Entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface AccountRepository extends CrudRepository<Account, Integer> {}
