package fab.ibm.ibmbank.Dao;

import fab.ibm.ibmbank.Dto.AccountDto;
import fab.ibm.ibmbank.Entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface AccountDao extends JpaRepository<Account, Integer> {
    Account findByIdAccount(int idAccount);
}
